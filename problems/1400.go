package problems

// 1313
// https://leetcode-cn.com/problems/decompress-run-length-encoded-list/
func DecompressRLEList(nums []int) []int {
	var res []int
	for i := 0; i < len(nums)-1; i += 2 {
		for j := 0; j < nums[i]; j++ {
			res = append(res, nums[i+1])
		}
	}
	return res
}

// 1342
// https://leetcode-cn.com/problems/number-of-steps-to-reduce-a-number-to-zero/
func NumberOfSteps(num int) int {
	var step = 0
	for num > 0 {
		step += 1 + num%2
		num /= 2
	}
	return step - 1
}

// 1351
// https://leetcode-cn.com/problems/count-negative-numbers-in-a-sorted-matrix/
func CountNegatives(grid [][]int) int {
	var num = 0
	var index = len(grid[0])
	for k, v := range grid {
		for i := 0; i < index; i++ {
			if v[i] < 0 {
				index = i
				break
			}
		}
		if index > 0 {
			num += len(v) - index
		} else {
			num += len(v) * (len(grid) - k)
			break
		}
	}
	return num
}

// 1365
// https://leetcode-cn.com/problems/how-many-numbers-are-smaller-than-the-current-number/
func SmallerNumbersThanCurrent(nums []int) []int {
	var arr = [101]int{}
	for _, v := range nums {
		arr[v]++
	}
	for i := 1; i <= 100; i++ {
		arr[i] += arr[i-1]
	}
	var res = make([]int, len(nums))
	for k, v := range nums {
		if v == 0 {
			res[k] = 0
		} else {
			res[k] = arr[v-1]
		}
	}
	return res
}

// 1389
// https://leetcode-cn.com/problems/create-target-array-in-the-given-order/
func CreateTargetArray(nums []int, index []int) []int {
	for i := 0; i < len(index); i++ {
		for j := i - 1; j >= 0; j-- {
			if index[j] >= index[i] {
				index[j]++
			}
		}
	}
	var res = make([]int, len(nums))
	for k, v := range nums {
		res[index[k]] = v
	}
	return res
}
