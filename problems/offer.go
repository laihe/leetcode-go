package problems

// 05
// https://leetcode-cn.com/problems/ti-huan-kong-ge-lcof/
func ReplaceSpace(s string) string {
	var res []rune
	for _, v := range s {
		if v == ' ' {
			res = append(res, []rune{'%', '2', '0'}...)
		} else {
			res = append(res, v)
		}
	}
	return string(res)
}

// 06
// https://leetcode-cn.com/problems/cong-wei-dao-tou-da-yin-lian-biao-lcof/
func ReversePrint(head *ListNode) []int {
	if head == nil {
		return nil
	}
	if head.Next != nil {
		return append(ReversePrint(head.Next), head.Val)
	}
	return []int{head.Val}
}
