package problems

// https://leetcode-cn.com/problems/volume-of-histogram-lcci/
func Trap2(height []int) int {
	var left, right = 0, len(height) - 1
	var area, level = 0, 0
	for left < right {
		if height[left] <= level {
			area += level - height[left]
			left++
		} else if height[right] <= level {
			area += level - height[right]
			right--
		} else {
			if height[left] <= height[right] {
				level = height[left]
				left++
			} else {
				level = height[right]
				right--
			}
		}
	}
	return area
}
