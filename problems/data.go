package problems

import (
	"strconv"
	"strings"
)

//链表结构
type ListNode struct {
	Val  int
	Next *ListNode
}

type NestedInteger struct {
}

func (ni NestedInteger) IsInteger() bool           { return true }
func (ni NestedInteger) GetInteger() int           { return 0 }
func (ni *NestedInteger) SetInteger(value int)     {}
func (ni *NestedInteger) Add(elem NestedInteger)   {}
func (ni NestedInteger) GetList() []*NestedInteger { return nil }

// https://leetcode-cn.com/problems/employee-importance/
type Employee struct {
	Id           int
	Importance   int
	Subordinates []int
}

//将切片转为链表
func SliceToListNode(s []int) *ListNode {
	var list, ptr *ListNode
	for _, v := range s {
		if ptr == nil {
			ptr = &ListNode{v, nil}
			list = ptr
		} else {
			ptr.Next = &ListNode{v, nil}
			ptr = ptr.Next
		}
	}
	return list
}

//判断两个链表所有节点的值是否相等
func (l *ListNode) Equal(l1 *ListNode) bool {
	var l1List = l
	var l2List = l1
	for l1List != nil {
		if l2List == nil || l1List.Val != l2List.Val {
			return false
		}
		l1List = l1List.Next
		l2List = l2List.Next
	}
	return l2List == nil
}

//打印链表所有的值
func (l *ListNode) String() string {
	var list = l
	var str = make([]string, 0)
	for list != nil {
		str = append(str, strconv.Itoa(list.Val))
		list = list.Next
	}
	return strings.Join(str, ",")
}

//二叉树
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

//打印链表所有的值
func (t *TreeNode) String() string {
	var str = make([]string, 0)
	var appendT func(tn *TreeNode)
	appendT = func(tn *TreeNode) {
		if tn != nil {
			appendT(tn.Left)
			str = append(str, strconv.Itoa(tn.Val))
			appendT(tn.Right)
		}
	}
	appendT(t)
	return strings.Join(str, ",")
}

//完美二叉树
type PerfectTreeNode struct {
	Val   int
	Left  *PerfectTreeNode
	Right *PerfectTreeNode
	Next  *PerfectTreeNode
}

//将切片转为链表
func SliceToTreeNode(s []int, nilVal int) *TreeNode {
	if len(s) == 0 {
		return nil
	}
	var stack = make([]*TreeNode, len(s))
	var pIndex = 0
	for k, v := range s {
		var node *TreeNode
		if v != nilVal {
			node = &TreeNode{Val: v}
		}
		stack[k] = node
		if k > 0 {
			if k%2 == 1 {
				if stack[pIndex] != nil {
					stack[pIndex].Left = node
				}
			} else {
				if stack[pIndex] != nil {
					stack[pIndex].Right = node
				}
				pIndex++
			}
		}
	}
	return stack[0]
}

func TreeNodeToSlice(root *TreeNode) []int {
	var s []int
	var stack []*TreeNode
	if root != nil {
		stack = append(stack, root)
	}
	for len(stack) > 0 {
		var node = stack[0]
		if node.Left != nil {
			stack = append(stack, node.Left)
		}
		if node.Right != nil {
			stack = append(stack, node.Right)
		}
		s, stack = append(s, node.Val), stack[1:]
	}
	return s
}

//N叉树
type ChildNode struct {
	Val      int
	Children []*ChildNode
}

type Node struct {
	Val    int
	Next   *Node
	Random *Node
}
